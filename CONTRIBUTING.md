# Contributing to Graphene nexus-client

Thanks for your interest in this project.

## Project description

This project provides the storage service for a the nexus repository 

* https://gitlab.eclipse.org/eclipse/graphene/nexus-client

## Developer resources

The project maintains the following source code repositories

* https://gitlab.eclipse.org/eclipse/graphene/nexus-client

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the Eclipse Foundation Webdev team via webdev@eclipse-foundation.org.
